/*-----------------------------------------------------------
* Name             : Fundraise Genius
* Author           : beingeorge
* Version          : 1.0
* Created          : Dec 2020
* File Description : Main Js file of the template
*------------------------------------------------------------
*/

! function($) {
    "use strict";

    /* ---------------------------------------------- /*
    *   Navbar
    /* ---------------------------------------------- */
    
    $('.navbar-toggler').on('click', function(){
        $("body").toggleClass('aside-open');
        $(this).toggleClass('active');
    });

}(window.jQuery);

